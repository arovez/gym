
<script>window.onload = function(){ window.print(); };</script>
<style>
@media print {
  @page { margin: 0; }
  body { margin: 1.6cm; }
}

table {
  border-collapse: collapse;
}

table, th, td {
  border: 1px solid black;
  padding: 10px;
}

</style>
<table>
<tr>
	<td colspan="4"><img src='/gym/img/pdflogo.png' /></td>
</tr>
<tr>
<td>PERIOD</td>
<td>TIME</td>
<td>FOOD PLAN</td>
<td>NOTES</td>
</tr>
<?php
			if(!empty($nutrition_data))
			{
				foreach($nutrition_data as $data=>$row)
				{				
					foreach($row as $r)
					{
						if(is_array($r))
						{
							$days_array[$data]["start_date"] = $row["start_date"];
							$days_array[$data]["expire_date"] = $row["expire_date"];
							$day = $r["day_name"];
							$days_array[$data][$day][] = $r;
						}
					}					
				}
				
				foreach($days_array as $data=>$row)
				{
					foreach($row as $day=>$value)
						{
							if(is_array($value))
							{ 
							?>
								<?php foreach($value as $r)
									{?>
									<tr>
	<td colspan="4"></td>
</tr>
									<tr>
										<td><?php echo $r['nutrition_time'] ?></td>
										<td><?php echo $r['nutrition_time_period'] ?></td>
										<td><?php echo $r['nutrition_value'] ?></td>
										<td><?php echo $r['nutrition_notes'] ?></td>
									</tr>
								<?php } ?>
							<?php } 
						}?>
			  <?php } 
			}else{
				echo "<td colspan='4'><i>No record found.</i></td>";
			}?>	
<tr>
</tr>
</table>

<?php if(!empty($nutrition_data)) { ?>
<table style="margin-top: 10px">
<tr>
	<th>Notes</th>
</tr>
<tr>
<td><?php echo $nutrition_data[$nutritionId]['notes']; ?></td>
</tr>
</table>
<?php } ?>
<?php die; ?>