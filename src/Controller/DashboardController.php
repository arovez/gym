<?php
namespace App\Controller;
use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;
use App\Controller\DateTime;
use GoogleCharts;

class DashboardController extends AppController
{
	public function initialize()
	{
		parent::initialize();
		$this->loadComponent("GYMFunction");
		require_once(ROOT . DS .'vendor' . DS  . 'chart' . DS . 'GoogleCharts.class.php');		
	}
	public function index()
	{	
		$session = $this->request->session()->read("User");
		switch($session["role_name"])
		{
			CASE "administrator":
				return $this->redirect(["action"=>"adminDashboard"]);
			break;
			CASE "member":
				return $this->redirect(["action"=>"memberDashboard"]);
			break;
			default:	
				return $this->redirect(["action"=>"staffAccDashboard"]);
		}		
	}
	
	public function adminDashboard()
	{
		$session = $this->request->session()->read("User");
		$conn = ConnectionManager::get('default');
		$this->autoRender = false;
		$mem_table = TableRegistry::get("GymMember");
		$grp_tbl = TableRegistry::get("GymGroup");
		$message_tbl = TableRegistry::get("GymMessage");	
		
		$members = $mem_table->find("all")->where(["role_name"=>"member"]);
		$members = $members->count();
		
		$staff_members = $mem_table->find("all")->where(["role_name"=>"staff_member"]);
		$staff_members = $staff_members->count();
		
		$curr_id = intval($session["id"]);
		$messages = $message_tbl->find("all")->where(["receiver"=>$curr_id]);
		$messages = $messages->count();
		
		$groups = $grp_tbl->find("all");
		$groups = $groups->count();
		
		$this->set("members",$members);
		$this->set("staff_members",$staff_members);
		$this->set("messages",$messages);
		$this->set("groups",$groups);
		
		$this->render("dashboard");
	}
	
	public function memberDashboard()
	{
		$session = $this->request->session()->read("User");
		$uid = intval($session["id"]);
		$conn = ConnectionManager::get('default');		
		$mem_table = TableRegistry::get("GymMember");
		$grp_tbl = TableRegistry::get("GymGroup");
		$message_tbl = TableRegistry::get("GymMessage");	
		
		$members = $mem_table->find("all")->where(["role_name"=>"member","id"=>$uid]);
		$members = $members->count();
		
		$staff_members = $mem_table->find("all")->where(["role_name"=>"staff_member"]);
		$staff_members = $staff_members->count();
		
		$curr_id = $uid;
		$messages = $message_tbl->find("all")->where(["receiver"=>$curr_id]);
		$messages = $messages->count();
		
		$groups = $grp_tbl->find("all");
		$groups = $groups->count();
		
		
		$this->set("members",$members);
		$this->set("staff_members",$staff_members);
		$this->set("messages",$messages);
		$this->set("groups",$groups);	
		
	}
	
	public function staffAccDashboard()
	{
		$session = $this->request->session()->read("User");
		$uid = intval($session["id"]);
		$conn = ConnectionManager::get('default');		
		$mem_table = TableRegistry::get("GymMember");
		$grp_tbl = TableRegistry::get("GymGroup");
		$message_tbl = TableRegistry::get("GymMessage");	
		
		if($session['role_name'] == "staff_member")
		{
		$members = $mem_table->find("all")->where(["role_name"=>"member","assign_staff_mem"=>$uid]);
		$members = $members->count();
		}
		else{
			$members = $mem_table->find("all")->where(["role_name"=>"member"]);
			$members = $members->count();
		}

		if($session['role_name'] == "staff_member")
		{
		$staff_members = $mem_table->find("all")->where(["role_name"=>"staff_member","id"=>$uid]);
		}
		else{
			$staff_members = $mem_table->find("all")->where(["role_name"=>"staff_member"]);
		}
		$staff_members = $staff_members->count();
		
		$curr_id = $uid;
		$messages = $message_tbl->find("all")->where(["receiver"=>$curr_id]);
		$messages = $messages->count();
		
		$groups = $grp_tbl->find("all");
		$groups = $groups->count();
		
		$this->set("members",$members);
		$this->set("staff_members",$staff_members);
		$this->set("messages",$messages);
		$this->set("groups",$groups);
		
	}

	public function getCalendarData()
	{
		$session = $this->request->session()->read("User");
		$res_tbl = TableRegistry::get("gym_reservation");
		$mem_table = TableRegistry::get("GymMember");
		$grp_tbl = TableRegistry::get("GymGroup");
		$message_tbl = TableRegistry::get("GymMessage");
		$membership_tbl = TableRegistry::get("Membership");				
		$notice_tbl = TableRegistry::get("gym_notice");		
		
		$reservationdata = $res_tbl->find("all")->hydrate(false)->toArray();
		$cal_array = array();
		
		if(!empty($reservationdata))
		{
			foreach ($reservationdata as $retrieved_data){
				$start_time = str_ireplace([":AM",":PM"],[" AM"," PM"],$retrieved_data["start_time"]);
				$end_time = str_ireplace([":AM",":PM"],[" AM"," PM"],$retrieved_data["end_time"]);				
				$start_time = date("H:i:s", strtotime($start_time)); 
				$end_time = date("H:i:s", strtotime($end_time)); 
				
				$cal_array [] = array (
						'title' => $retrieved_data["event_name"],
						'start' => $retrieved_data["event_date"]->format("Y-m-d")."T{$start_time}",
						'end' => $retrieved_data["event_date"]->format("Y-m-d")."T{$end_time}",
						
				);
			}
		}
		
		$birthday_boys=$mem_table->find("all")->where(["role_name"=>"member","activated"=>1])->group("id")->hydrate(false)->toArray();
		$boys_list="";
		
		if (!empty($birthday_boys )) {
			foreach ( $birthday_boys as $boys ) {
				
				//$startdate = $boys["birth_date"]->format("Y");
				$startdate = date("Y",strtotime($boys["birth_date"]));
				$enddate = $startdate + 90;
				$years = range($startdate,$enddate,1);
				foreach($years as $year)
				{				
					$start = date('m-d',strtotime($boys['birth_date']));
					 $cal_array [] = array (
						'title' => $boys["first_name"]."'s Birthday",
						//'start' =>"{$year}-{$boys["birth_date"]->format("m-d")}",
						//'end' => "{$year}-{$boys["birth_date"]->format("m-d")}",
						'start' => "{$year}-{$start}",
						'end' => "{$year}-{$start}",
						'backgroundColor' => '#F25656');
				}
			}

		}
		##################################
		$all_notice = "";
		if($session["role_name"] == "administrator")
		{
			$all_notice = $notice_tbl->find("all")->hydrate(false)->toArray();
		}
		else{
			$all_notice = $notice_tbl->find("all")->where(["OR"=>[["notice_for"=>"all"],["notice_for"=>$session["role_name"]]]])->hydrate(false)->toArray();
		}
		
		if (! empty ( $all_notice )) {
			foreach ( $all_notice as $notice ) {
				$i=1;				
				$cal_array[] = array (
						'title' => $notice["notice_title"],
						'start' => $notice["start_date"]->format("Y-m-d"),
						'end' => date('Y-m-d',strtotime($notice["end_date"]->format("Y-m-d").' +'.$i.' days')),
						'color' => '#12AFCB'
				);	
				
			}
		}		
		return $cal_array;		
	}
	
	public function isAuthorized($user)
	{
		$role_name = $user["role_name"];
		$curr_action = $this->request->action;
		$admin_actions = ["index","adminDashboard","memberDashboard","staffAccDashboard"];
		$members_actions = ["index","memberDashboard"];
		$staff_acc_actions = ["index","staffAccDashboard"];
		switch($role_name)
		{
			CASE "administrator":
				if(in_array($curr_action,$admin_actions))
				{return true;}else{return false;}
			break;
			
			CASE "member":
				if(in_array($curr_action,$members_actions))
				{return true;}else{return false;}
			break;
			
			CASE "staff_member":
				if(in_array($curr_action,$staff_acc_actions))
				{return true;}else{ return false;}
			break;
			
			CASE "accountant":
				if(in_array($curr_action,$staff_acc_actions))
				{return true;}else{return false;}
			break;
		}
		
		return parent::isAuthorized($user);
	}
}